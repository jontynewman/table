<?php

namespace JontyNewman;

use ArrayAccess;
use EmptyIterator;
use Iterator;
use JontyNewman\Table\Driver;
use JontyNewman\Table\Driver\Exception as DriverException;
use JontyNewman\Table\Exception as TableException;
use PDO;
use PDOStatement;
use RuntimeException;
use Throwable;
use UnexpectedValueException;

/**
 * An array-accessible table of data.
 */
class Table implements ArrayAccess
{

    /**
     *
     * @var Driver
     */
    private $driver;

    /**
     *
     * @var object
     */
    private $pair;

    public static function hybrid(
            Driver $driver,
            Driver $backup,
            callable $sync = null
    ): Driver {

        return new class($driver, $backup, $sync) implements Driver {

            private $driver;
            private $backup;
            private $sync;

            public function __construct(
                    Driver $driver,
                    Driver $backup,
                    callable $sync = null
            ) {

                $this->driver = $driver;
                $this->backup = $backup;
                $this->sync = $sync;
            }

            public function select($offset)
            {

                $value = null;
                $prior = null;

                try {
                    $value = $this->driver->select($offset);
                } catch (Throwable $prior) {
                    // Do nothing (implicitly assign exception to previous).
                }

                if (is_null($value)) {
                    $value = $this->selectFromBackup($offset, $prior);
                }

                return $value;
            }

            public function insert($offset, $data): void
            {
                $this->backup->insert($offset, $data);
                $this->driver->insert($offset, $data);
            }

            public function update($offset, $data): void
            {
                $this->backup->update($offset, $data);
                $this->driver->update($offset, $data);
            }

            public function delete($offset): void
            {
                $this->backup->delete($offset);
                $this->driver->delete($offset);
            }

            private function selectFromBackup(
                    $offset,
                    Throwable $prior = null
            ) {

                try {

                    $value = $this->backup->select($offset);

                    // If both the backup value and synchronization are available...
                    if (!(is_null($this->sync) || is_null($value))) {
                        ($this->sync)();
                        $value = $this->driver->select($offset);
                    }

                    return $value;
                } catch (Throwable $previous) {
                    throw new TableException(
                            TableException::BACKUP,
                            $previous,
                            $prior
                    );
                }
            }
        };
    }

    public static function decode(Iterator $filepaths): iterable
    {

        return new class($filepaths) implements Iterator {

            private $iterator;

            public function __construct(Iterator $iterator)
            {

                $this->iterator = $iterator;
            }

            public function current()
            {

                $current = $this->iterator->current();

                $json = file_get_contents($current);

                if (false === $json) {
                    throw new UnexpectedValueException('Expected JSON but got FALSE');
                }

                return json_decode($json);
            }

            public function key()
            {

                return $this->iterator->key();
            }

            public function next(): void
            {

                $this->iterator->next();
            }

            public function rewind(): void
            {

                $this->iterator->rewind();
            }

            public function valid(): bool
            {

                return $this->iterator->valid();
            }
        };
    }

    public static function dir(ArrayAccess $directory): Driver
    {

        return new class($directory) implements Driver {

            private $directory;

            public function __construct(ArrayAccess $directory)
            {

                $this->directory = $directory;
            }

            public function select($offset)
            {

                $data = null;
                $json = '';
                $path = $this->directory->offsetGet($offset);

                if (file_exists($path)) {

                    $json = file_get_contents($path);

                    if (false === $json) {
                        throw new UnexpectedValueException('Expected file contents but got FALSE');
                    }

                    $data = json_decode($json);
                }

                return $data;
            }

            public function insert($offset, $data): void
            {

                $this->write($offset, $data);
            }

            public function update($offset, $data): void
            {

                $this->write($offset, $data);
            }

            public function delete($offset): void
            {

                $this->directory->offsetUnset($offset);
            }

            private function write($offset, $data): void
            {

                $json = json_encode($data, JSON_PRETTY_PRINT);

                if (false === $json) {
                    throw new UnexpectedValueException('Expected JSON but got FALSE');
                }

                $this->directory->offsetSet($offset, $json);
            }
        };
    }

    public static function pdo(
            PDO $pdo,
            string $table,
            string $primary,
            iterable $columns = null
    ): Driver {

        return new class($pdo, $table, $primary, $columns) implements Driver {

            /**
             *
             * @var PDO
             */
            private $pdo;
            private $table;
            private $primary;

            public function __construct(
                    PDO $pdo,
                    string $table,
                    string $primary,
                    iterable $columns = null
            ) {

                $this->pdo = $pdo;
                $this->table = $table;
                $this->primary = $primary;

                if (is_null($columns)) {
                    $columns = new EmptyIterator();
                }

                if (!is_array($columns)) {
                    $columns = iterator_to_array($columns, false);
                }

                $this->columns = $columns;
            }

            public function select($offset)
            {

                $sql = "{$this->toSelect()} WHERE {$this->primary} = ?";
                $statement = $this->execute($sql, [$offset]);
                $value = $statement->fetch(PDO::FETCH_OBJ);

                if (false === $value) {
                    $value = null;
                }

                $statement->closeCursor();

                return $value;
            }

            public function insert($offset, $data): void
            {

                $sql = '';
                $params = [];
                $filtered = $this->filter((array) $data);
                $array = [$this->primary => $offset] + $filtered;
                $columns = array_keys($array);
                $imploded = implode(', ', $columns);

                if ($columns) {
                    $imploded = " ({$imploded})";
                }

                $sql .= "INSERT INTO {$this->table}{$imploded}";
                $sql .= ' VALUES (';

                foreach ($array as $value) {

                    if ($params) {
                        $sql .= ', ';
                    }

                    $sql .= '?';
                    $params[] = $value;
                }

                $sql .= ')';

                $this->execute($sql, $params)->closeCursor();
            }

            public function update($offset, $data): void
            {

                $sql = '';
                $set = '';
                $assignments = [];
                $params = [];

                foreach ($this->filter((array) $data) as $key => $value) {
                    $assignments[] = "{$key} = ?";
                    $params[] = $value;
                }

                if ($assignments) {
                    $set .= ' SET (';
                    $set .= implode(', ', $assignments);
                    $set .= ')';
                }

                $sql .= "UPDATE {$this->table}";
                $sql .= $set;
                $sql .= " WHERE {$this->primary} = ?";

                $params[] = $offset;

                $statement = $this->execute($sql, $params);
                $count = $statement->rowCount();

                if (1 !== $count) {
                    throw new DriverException($count);
                }

                $statement->closeCursor();
            }

            public function delete($offset): void
            {

                $sql = "DELETE FROM {$this->table}";
                $sql .= " WHERE {$this->primary} = ?";

                $this->execute($sql, [$offset])->closeCursor();
            }

            private function toSelect(): string
            {

                $imploded = '';
                $columns = $this->columns;

                if (!$columns) {
                    $columns[] = '*';
                }

                $imploded .= implode(', ', $columns);

                return "SELECT {$imploded} FROM {$this->table}";
            }

            private function execute(
                    string $sql,
                    array $params = null
            ): PDOStatement {

                $options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
                $statement = $this->pdo->prepare($sql, $options);

                if (!$statement) {
                    throw new UnexpectedValueException('Expected successful statement preparation');
                }

                if (!$statement->execute($params)) {
                    throw new UnexpectedValueException('Expected successful statement execution');
                }

                return $statement;
            }

            private function filter(array $array)
            {

                if (!is_null($this->columns)) {
                    $filled = array_fill_keys($this->columns, null);
                    $array = array_intersect_key($array, $filled);
                }

                return $array;
            }
        };
    }

    public function __construct(object $driver)
    {
        $this->driver = $driver;
        $this->pair = self::toPair(null, null);
    }

    public function offsetExists($offset)
    {

        $this->pair = self::toPair($offset, $this->driver->select($offset));

        return !is_null($this->pair->value);
    }

    public function offsetGet($offset)
    {

        $value = null;

        if ($this->pair->offset === $offset || $this->offsetExists($offset)) {
            $value = $this->pair->value;
        }

        return $value;
    }

    public function offsetSet($offset, $value)
    {

        try {
            $this->driver->insert($offset, $value);
        } catch (Throwable $prior) {
            $this->update($offset, $value, $prior);
        }
    }

    public function offsetUnset($offset)
    {
        $this->driver->delete($offset);
    }

    private static function toPair($offset, $value): object
    {

        $pair = (object) null;

        $pair->offset = $offset;
        $pair->value = $value;

        return $pair;
    }

    private function update($offset, $data, Throwable $prior = null): void
    {

        try {
            $this->driver->update($offset, $data);
        } catch (Throwable $previous) {
            throw new TableException(TableException::UPSERT, $previous, $prior);
        }
    }
}

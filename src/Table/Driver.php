<?php

namespace JontyNewman\Table;

interface Driver {

	public function select($offset);

	public function insert($offset, $data): void;

	public function update($offset, $data): void;

	public function delete($offset): void;
}

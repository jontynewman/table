<?php

namespace JontyNewman\Table\Driver;

use RuntimeException;
use Throwable;

class Exception extends RuntimeException {

	const PREFIX = 'Expected 1 affected row during update but got ';

	public function __construct(int $count, Throwable $previous = null) {
		parent::__construct(self::toMessage($count), $count, $previous);
	}

	private static function toMessage(int $count) {

		$message = self::PREFIX;
		$message .= $count;

		return $message;
	}
}

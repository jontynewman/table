<?php

namespace JontyNewman\Table;

use RuntimeException;
use Throwable;

class Exception extends RuntimeException {

    const BACKUP = 1;

    const UPSERT = 2;

	private $prior;

	public function __construct(
            int $code,
			Throwable $previous = null,
			Throwable $prior = null
	) {
		parent::__construct(self::toMessage($code), 0, $previous);
		$this->prior = $prior;
	}

	public function getPrior(): Throwable {

		return $this->prior;
	}

    private static function toMessage(int $code): string {

        $message = '';

        switch ($code) {
            case self::BACKUP:
                $message = 'Failure during select from backup driver';
                break;
            case self::UPSERT:
                $message = 'Failure(s) during upsert';
                break;
        }

        return $message;
    }
}
